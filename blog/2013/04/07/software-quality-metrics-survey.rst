============================
A Survey on Software Quality
============================

Want to help me graduate, and make the software industry a better place for us all?

Great!  I am writing my graduate report on the topic of software quality, metrics, and visualization.
The report will include a write-up on the results of the survey distributed here,
an evaluation of:

* how software-industry professionals define software quality
* how software methods, metrics, and visualizations are used within teams for software quality improvement

The survey is about 25 questions long and takes about 10 minutes
to complete.  You can find the survey at:

**Edit April 10, 2013:** The survey is now closed, thanks to all that participated!

I would appreciate your help in distributing this survey to others you know in
the software industry who would be interested in helping with this research on
software quality.  Results from the survey will be posted here next month.

If interested in reading more on the subject, I encourage you to take a look
at my `previous posts about metrics </tags/metrics/>`_, including a case study of Django and
cyclomatic complexity metrics for Python code.

Thank you!  
