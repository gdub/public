==============================
Dude, where's my color picker?
==============================

It's not something I use too terribly often, but today I found myself missing the KDE color picker applet, a tiny applet that gives you an eye dropper you can use to quickly sample any color on your screen and copy its hex value to the clipboard.  I could have sworn it was available in Kubuntu Gutsy by default.  Turns out that, in Hardy at least, it comes as part of the ``kicker-applets`` package.
