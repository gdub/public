=================================
Eclipse security component errors
=================================

Just got through installing eclipse on an Kubuntu Hardy box, and got the following error during the splash screen:


    Could not initialize the application's security component. The most likely cause is problems with files in your application's profile directory. Please check that this directory has no read/write restrictions and your hard disk is not full or close to full. It is recommended that you exit the application and fix the problem. If you continue to use this session, you might see incorrect application behavior when accessing security features.

The solution seems to be to create the directory: ``~/.mozilla/eclipse``

The corresponding bugs where the above solution was found:

* `Ubuntu bug <https://bugs.launchpad.net/ubuntu/+source/eclipse/+bug/188380>`_
* `Eclipse bug <https://bugs.eclipse.org/bugs/show_bug.cgi?id=206432>`_
