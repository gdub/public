==========================
News sites that don't suck
==========================

I sure wish there were news sites in my city that didn't suck.  How happy I would be if I could have sites such as the `Django <http://www.djangoproject.com>`_-powered `49abcnews <http://www.49abcnews.com/>`_ or `LJWorld <http://www.ljworld.com/>`_ to give my attention to each evening.  Instead, I'm stuck with `crappy news site`__ after `crappy news site`__ after `crappy news site`__.  Popups, popovers, registrations, and URLs that make you cringe...

.. __: http://www.news8austin.com/
.. __: http://www.statesman.com/
.. __: http://www.kvue.com/

::
    
    http://sports.statesman.com/merge/tsnform.aspx?c=statesman&amp;page=mlb/news/index.aspx?sport=AA
    http://www.kvue.com/news/top/stories/051906cckrKvurescape.2b620a4.html

Oh my!

To be fair, `Statesman.com <http://www.statesman.com>`_ (and its `Austin360.com <http://www.austin360.com>`_ entertainment site) is now much more pleasing on the eye due to a major redesign last December.  The redesign seems to have paid off, with the Statesman's web site duo recently receiving the top ranking by the `Newspaper Association of America (NAA) <http://www.naa.org/>`_ for the greatest local reach among large newspaper sites.  With a monthly readership of 31 percent of Austinites, it edged out `The Washington Post <http://www.washingtonpost.com>`_ (27 percent) and the `San Diego Union-Tribune <http://www.signonsandiego.com>`_ (23 percent).

In my mind though, Statesman.com fails to deliver the news.  All those headlines on the home page are just a tease to get me to register.  I just want to be able to read the news and `find Rolexes in supermarkets <http://www.timesonline.co.uk/article/0,,2-1459390,00.html>`_ without getting hassled.  Is that so much to ask?

LJWorld gets it.  There's plenty of content here just begging to be Django-ed.  With Austin being the state's capital, land of the
`silicon hills <http://www.siliconmaps.com/hills3.html>`_,
a `smart place to live <http://www.kiplinger.com/personalfinance/features/archives/2006/05/intro.html>`_,
home to `one of the largest universities <http://www.utexas.edu>`_ in the U.S.
(with plenty of `college girls making out (04:43) <http://video.google.com/videoplay?docid=-70449010942275062>`_),
and then there's always the `Live Music Capital of the World <http://www.austintexas.org/music/>`_.
Austin news sites don't get it.
