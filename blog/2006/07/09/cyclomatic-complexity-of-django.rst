===============================
Cyclomatic complexity of Django
===============================

`Adrian's recent post <http://groups.google.com/group/django-developers/browse_thread/thread/1026181083385c58/>`_ on the dev mailing list about the code within ``django.db.models`` got me remembering some other "monstrous" functions I've seen while browsing the Django source.  Guess no more as to the most monstrous functions of Django because below are the results of my `previously posted complexity.py script <http://gdub.wordpress.com/2006/07/09/cyclomatic-complexity-for-python-code/>`_ when run on the Django source.

Here are all 80 of the functions and methods of Django that are of moderate risk or higher::

    Showing functions/methods with complexity greater than or equal to 11:

    Filename                                    Function/Method                              Lines of Code Complexity
    ./core/management.py                        get_validation_errors                                  146         78
    ./db/models/manipulators.py                 AutomaticManipulator.save                               97         43
    ./contrib/comments/templatetags/comments.py DoCommentForm.__call__                                  51         31
    ./views/i18n.py                             javascript_catalog                                      68         28
    ./contrib/admin/views/main.py               change_stage                                            75         28
    ./utils/translation/trans_real.py           translation                                             62         26
    ./bin/make-messages.py                      make_messages                                          114         26
    ./contrib/admin/views/main.py               _get_deleted_objects                                    65         25
    ./core/handlers/base.py                     BaseHandler.get_response                                50         24
    ./views/defaults.py                         shortcut                                                40         23
    ./contrib/auth/create_superuser.py          createsuperuser                                         64         23
    ./core/management.py                        inspectdb                                               84         23
    ./contrib/admin/views/doc.py                model_detail                                            64         22
    ./templatetags/i18n.py                      do_block_translate                                      41         21
    ./core/management.py                        syncdb                                                  61         21
    ./core/management.py                        execute_from_command_line                               74         21
    ./views/generic/create_update.py            update_object                                           54         20
    ./contrib/admin/views/decorators.py         staff_member_required                                   44         20
    ./utils/simplejson/decoder.py               scanstring                                              41         19
    ./contrib/admin/views/main.py               add_stage                                               51         19
    ./core/validators.py                        RelaxNGCompact.__call__                                 51         19
    ./db/models/query.py                        lookup_inner                                           107         19
    ./db/models/query.py                        delete_objects                                          48         19
    ./template/__init__.py                      TokenParser.value                                       61         18
    ./utils/simplejson/decoder.py               JSONObject                                              37         18
    ./utils/decorators.py                       decorator_from_middleware                               25         18
    ./contrib/comments/templatetags/comments.py DoGetCommentList.__call__                               31         18
    ./contrib/comments/views/comments.py        post_comment                                            66         18
    ./utils/translation/trans_real.py           get_language_from_request                               40         17
    ./core/mail.py                              send_mass_mail                                          31         17
    ./views/generic/create_update.py            create_object                                           38         16
    ./views/static.py                           serve                                                   28         16
    ./core/servers/fastcgi.py                   runfastcgi                                              56         16
    ./core/management.py                        get_sql_delete                                          49         16
    ./db/models/query.py                        QuerySet.__getitem__                                    38         16
    ./utils/simplejson/encoder.py               floatstr                                                17         15
    ./utils/simplejson/encoder.py               JSONEncoder._iterencode_dict                            49         15
    ./views/generic/create_update.py            delete_object                                           36         15
    ./contrib/comments/templatetags/comments.py DoCommentCount.__call__                                 25         15
    ./db/models/fields/related.py               create_many_related_manager                             73         15
    ./db/models/query.py                        QuerySet._get_sql_clause                                54         15
    ./middleware/common.py                      CommonMiddleware.process_request                        22         14
    ./template/__init__.py                      Parser.parse                                            35         14
    ./template/defaulttags.py                   do_if                                                   33         14
    ./template/defaulttags.py                   cycle                                                   25         14
    ./views/generic/date_based.py               object_detail                                           43         14
    ./contrib/comments/views/comments.py        post_free_comment                                       41         14
    ./contrib/admin/templatetags/admin_list.py  items_for_result                                        59         14
    ./contrib/admin/views/main.py               get_javascript_imports                                  23         14
    ./contrib/admin/views/main.py               ChangeList.get_query_set                                39         14
    ./core/handlers/base.py                     BaseHandler.load_middleware                             32         14
    ./db/models/base.py                         Model.__init__                                          33         14
    ./template/defaulttags.py                   SsiNode.render                                          21         13
    ./views/generic/list_detail.py              object_list                                             46         13
    ./contrib/auth/handlers/modpython.py        authenhandler                                           28         13
    ./db/models/fields/__init__.py              Field.get_manipulator_fields                            24         13
    ./db/models/base.py                         ModelBase.__new__                                       26         13
    ./dispatch/saferef.py                       safeRef                                                 65         12
    ./dispatch/dispatcher.py                    _removeOldBackRefs                                      20         12
    ./template/loader.py                        find_template_source                                    26         12
    ./template/defaulttags.py                   IfNode.render                                           21         12
    ./utils/timesince.py                        timesince                                               34         12
    ./views/generic/date_based.py               archive_month                                           36         12
    ./core/management.py                        _get_sql_model_create                                   44         12
    ./conf/__init__.py                          Settings.__init__                                       26         12
    ./db/models/fields/__init__.py              DateTimeField.to_python                                 14         12
    ./db/models/related.py                      RelatedObject.get_list                                  21         12
    ./db/models/options.py                      Options.has_field_type                                  16         12
    ./dispatch/dispatcher.py                    connect                                                 34         11
    ./template/defaultfilters.py                pluralize                                               18         11
    ./utils/simplejson/decoder.py               JSONArray                                               21         11
    ./views/generic/list_detail.py              object_detail                                           33         11
    ./views/generic/date_based.py               archive_week                                            31         11
    ./views/generic/date_based.py               archive_day                                             34         11
    ./contrib/redirects/middleware.py           RedirectFallbackMiddleware.process_response             17         11
    ./contrib/admin/templatetags/log.py         DoGetAdminLog.__call__                                  11         11
    ./db/backends/postgresql/base.py            DatabaseWrapper.cursor                                  21         11
    ./db/backends/postgresql_psycopg2/base.py   DatabaseWrapper.cursor                                  21         11
    ./db/models/fields/related.py               ReverseSingleRelatedObjectDescriptor.__get__            19         11
    ./db/models/query.py                        get_where_clause                                        18         11
