==================================
Texas Python Regional Unconference
==================================

Just a quick announcement for all the Houston and surrounding area Python lovers out there.  There is a `Texas Python Regional Unconference <http://pycamp.python.org/Texas/HomePage>`_ being held in a couple weeks (Sept. 15 - 16) at the University of Houston main campus.  Registration is free and as simple as adding your name to the `registration wiki page <http://pycamp.python.org/Texas/Registration>`_.  I'll be there to talk or hack Django with anyone interested.  See you there!
