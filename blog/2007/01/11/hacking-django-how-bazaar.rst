==========================
Hacking Django, how Bazaar
==========================

A short while ago, I went on a mission to find a better way to hack `Django <http://www.djangoproject.com/>`_.  I needed a way to keep my patches organized and updated.  So, in this article I discuss my experiences with a tool I found to help me achieve these goals...

Enter `Bazaar <http://bazaar-vcs.org/>`_, one of the several `distributed version control systems <http://en.wikipedia.org/wiki/Version_control_system#Distributed_version_control>`_ that have been getting some hype recently.  With `so many to choose from <http://en.wikipedia.org/wiki/Comparison_of_revision_control_software>`_, why did I choose Bazaar?  Well, maybe it was because of the cool name, or maybe it was the pretty website, or maybe just because it's written in the language I love—`Python <http://www.python.org/>`_.  If you are familiar with `Subversion <http://subversion.tigris.org/>`_, then you will feel right at home with Bazaar.  This is because Bazaar uses most of the same commands as Subversion (e.g. ``svn ci = bzr ci``, ``svn st = bzr st``, etc.).

Following `this guide for hacking an open source project using bzr <http://jameswestby.net/tips/tips/hacking-a-project-with-bzr.html>`_, I created a ``django`` directory to stash my django hacks.  After using svn to check out the latest Django code to a directory named ``upstream``, and turning ``upstream`` into a bzr repository too, it was time to start the real work.  To re-iterate in command line speak, here is what I did:

**Update:** The steps below have been updated to reflect changes with bzr commands (versions >=0.15rc2).

::
    
    mkdir django
    cd django
    bzr init-repo .            # use "bzr init-repo --trees ." here instead if using a version <0.15rc2
    svn co http://code.djangoproject.com/svn/django/trunk/ upstream
    cd upstream
    bzr init
    echo ".svn" > .bzrignore   # do not store svn metadata in our bzr repo
    bzr add
    bzr ci -m 'Initial import of Django.'

Usually, when you want to add a new feature to some code, you create a branch.  The purpose of these, so called, feature branches is to keep the features separated.  Trying to create multiple features in the same branch would just be a big mess.  Creating branches in a Subversion repository is easy enough, you just ``svn cp`` the code to a new location.  It's the keeping your branch in sync with the trunk that's not as easy.

With Subversion, you have to keep track of the revisions that have already been merged to your branch and make sure to only merge the changes that haven't already been merged.  Not so with Bazaar; Bazaar keeps track of this for you.  A simple ``bzr merge`` will pull in upstream changes that have not been merged yet.

So, when I wanted to start working on a patch for `ticket #2473 <http://code.djangoproject.com/ticket/2473>`_, I created a branch::

    bzr branch upstream in-and-empty-list-2473

After ``cd``'ing into my new branch and committing my changes, I created a diff to attach to the ticket with the command::

    bzr diff -r branch:../upstream

So far, working with Bazaar has been a breeze.  Without it, `my`__ `Django`__ `holiday`__ `hacking`__ wouldn't have been nearly as fun.

.. __: http://code.djangoproject.com/ticket/2473
.. __: http://code.djangoproject.com/ticket/2756
.. __: http://code.djangoproject.com/ticket/3180
.. __: http://code.djangoproject.com/ticket/3182
