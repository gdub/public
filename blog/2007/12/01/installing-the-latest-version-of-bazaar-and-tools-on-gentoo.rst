===========================================================
Installing the latest version of Bazaar and tools on Gentoo
===========================================================

Currently, the ``bzr-svn`` and ``bzr-gtk`` plugins are not available in Gentoo's package directory.  Fortunately, there is exists a `bzr Gentoo overlay <https://launchpad.net/bzr-gentoo-overlay/>`_. If you've never used an `overlay <http://www.gentoo.org/proj/en/overlays/userguide.xml>`_ before, it's a way to override or add packages to Gentoo's standard package directory.  In this overlay there are a couple common plugins, as well as a patched version of Subversion that is needed for the ``bzr-svn`` plugin.

Current packages in the overlay:

* ``bzr``
* ``bzrtools``
* ``bzr-config``
* ``bzr-dbus``
* ``bzr-gtk``
* ``bzr-rebase``
* ``bzr-svn``
* ``bzr-vimdiff``
* ``paramiko``
* ``subversion`` (patched version needed for bzr-svn)

Steps for using the overlay:

1. As usual, update your portage tree::

       emerge --sync

2. Make sure you have the following line in your ``/etc/make.conf``::

       source /usr/portage/local/layman/make.conf

3. Get a copy of the overlay from launchpad::

       bzr branch lp:///bzr-gentoo-overlay

4. Make sure you have the path to the overlay set in ``/usr/portage/local/layman/make.conf``::

       PORTDIR_OVERLAY="/path/to/bzr-gentoo-overlay"

5. Now you should be able to update your system, pulling in updates to any currently installed packages from the new overlay::

       emerge -uav world

6. emerge any of the new packages you want that are now available from the overlay.  Note: If you install ``bzr-svn``, make sure to re-install Subversion so that the patched version is used from the overlay::

       emerge -av bzr-gtk bzr-svn subversion
