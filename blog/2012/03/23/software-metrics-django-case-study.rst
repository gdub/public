=====================================
Software Metrics: A Django Case Study
=====================================


With the permission of the other authors, I'd like to share with you a couple of reports written for an assignment in the Software Measurement and Metrics graduate course I took last semester.
For this project, our class split into two teams that each performed their own case study on the `Django project <https://www.djangoproject.com/>`_.
The reports were submitted in November 2011, and thus analyzed code and bug data through about the end of October 2011.

While I have `previously analyzed Django's complexity </blog/2006/cyclomatic-complexity-of-django/>`_ (using a `hack of some existing complexity scripts </blog/2006/cyclomatic-complexity-for-python-code/>`_), the case studies presented here analyze several aspects of the Django project, including code size, code complexity, component quality, and test coverage.

Reports
=======
Below you will find excerpts describing the motivations for each report, followed by links to the full PDF versions of the reports.

Team A
------
Written by Eric Huneke, Brendan Long, and Gary Wilson Jr.

      This report describes our case study of Django, an open source web application framework written in Python. The overall goal of our study was a data-driven exploration of Django's code base, issue tracker, and wiki space, targeted to answer our research questions related to code size, complexity, and defect counts. We explored both the current state and the history of the Django project's artifacts, and produced several visualizations of the most interesting data.

`Download report </blog/2012/03/23/django_case_study_team_a.pdf>`__ (815K, 19 pages).


Team B
------
Written by Kevin Boos, Richard Campbell, Evan Grim, and Hasanain A. Jwair.

      Our motivation for conducting this case study, aside from the obvious assignment requirements, is to answer a variety of research questions corresponding to several standard software metrics: code size, modularity, complexity, unit test coverage, and component quality relative to defect quantity. We believe that obtaining and analyzing data from the Django repositories and databases will allow us to generate graphical representations of important relationships between developer behavior, component organization, testing and debugging procedures, and a wide variety of other project characteristics. From these relationships, we can then draw conclusions about how to improve the software development process and other facets of the Django project.

`Download report </blog/2012/03/23/django_case_study_team_b.pdf>`__ (2.4M, 25 pages).


Conclusions
===========

I encourage you to browse through the reports and their visualizations.  For the impatient, the teams' conclusions were as follows:

Team A
------

      This report presented the data we collected as part of a case study examining various aspects of Django, an open source web application framework. We found that the codebase has been growing at a steady rate since its start and will likely continue to grow. Overall complexity has been manageable, with just a few files containing egregiously complex functions. A refactoring effort focused on the files presented in table 4.1 is recommended. Ticket data relating to Django reflect basic intuitions about how they should behave. We were able to get a rough measure of component quality by looking at the number of tickets relating to each component, and showed that the top three components by ticket count (documentation, database layer, and contrib.admin) accounted for over 40% of all reported tickets. Test coverage of Django's components is high, with most components having at least 95% coverage. Coverage has trended upward for almost the entire project's history, and saw an extreme increase in the time periods before its 1.0 release.

Team B
------

      This case study of the Django project, an open-source web development framework composed in Python, has presented a wide variety of statistically-significant data. To measure code size, rate of growth, complexity, and maintainability, we used Django's source control repository to analyze historical revisions since 2005. To analyze defect tracking, we looked at ticket records dating to the beginning of the Django project. For unit testing coverage, coding standards, and style guidelines analysis, we analyzed source code that was extracted from the Django subversion server on October 31, 2011.

      We found that the size of the project has greatly increased since its start, but that the rate of increase has decreased over time. The largest component with approximately half of the code is the contrib component which could be broken up into smaller, more manageable components. The overall cyclomatic complexity of Django is 2.49 which is respectable. Complexity started out nice and increased for the first few years, but has decreased since the initial increase. Some modules, though, have high complexities which should be reduced if possible.

      The ticket data showed that the majority of the tickets opened were for defects. When looking at only the defect tickets, we found 4 components out of 48 had almost 60% of the defects opened against them. The core and database components have the most defects per SLOC, so improving code quality in these areas will greatly reduce the overall number of defects in the Django project.

      As a whole, we found that the Django code is well covered by its test cases. The component with the lowest coverage, contrib.admin, was also found to be one of the components with the highest number of defects opened against it. Improving test coverage may reduce the number of tickets opened.

      We found that python coding standards and style conventions are poorly followed by Django developers. Additionally, Django code is poorly commented, only 7% of the total lines of the code base were comment lines. We also found that the amount of documentation is not consistent throughout the code elements, while most of the methods are documented, less than half the modules and classes have documentations.

