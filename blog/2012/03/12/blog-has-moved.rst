==============
Blog has moved
==============

Thanks to some time at `PyCon <https://us.pycon.org/>`_, I was finally able to move my blog off `Wordpress <http://wordpress.com/>`_ and onto a new domain at:

    http://thegarywilson.com/

The new site is powered with an `nginx <http://nginx.org/>`_ / `gunicorn <http://gunicorn.org/>`_ / `Django <http://djangoproject.com>`_ / `PostgreSQL <http://www.postgresql.org/>`_ stack.  Using Wordpress' `Site Redirect <http://en.support.wordpress.com/site-redirect/>`_ feature, I was able to 301 redirect all the existing content to the new domain.

