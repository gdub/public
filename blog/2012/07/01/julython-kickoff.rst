================
Julython Kickoff
================

Early this morning, a handful of developers here in `Austin, TX <http://www.julython.org/location/austin-tx/>`_ got together to kickoff `Julython <http://www.julython.org/>`_ over at `Bennu Coffee <http://www.bennucoffee.com/>`_.

The idea of Julython is to work a little each day on the project(s) of your choice, and is based on `National Novel Writing Month (a.k.a. NaNoWriMo) <http://en.wikipedia.org/wiki/National_Novel_Writing_Month>`_.  Getting started for Julython is easy, just head to http://www.julython.org/, login with your twitter account, `setup your e-mail address and web hooks <http://www.julython.org/help/>`_, hack, hack, hack, and start `racking up points <http://www.julython.org/location/>`_.

I kicked off my Julython with a bug fix for `python-xlsx <https://github.com/staale/python-xlsx>`_ and a couple enhancements to `python-archive <https://github.com/gdub/python-archive>`_.  Incidentally, I'll also be earning some points for this blog post, seeing as how it makes use of a `git repo <https://bitbucket.org/gdub/public>`_ for its backend.

