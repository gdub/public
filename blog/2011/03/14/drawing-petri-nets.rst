==================
Drawing Petri nets
==================

I recently went looking for tools to easily draw `Petri nets <http://en.wikipedia.org/wiki/Petri_net>`_, a modeling language for distributed systems, and here's what I found:

* `petri-nets LaTeX package <http://www.ctan.org/tex-archive/macros/generic/petri-nets/>`_: A LaTeX package written by Franck Pommereau specifically for drawing Petri nets that makes use of `PSTricks <http://www.tug.org/PSTricks/main.cgi/>`_ as a basis for the actual creation of the drawings. I didn't actually try this package out because, after skimming through the documentation, it seems that nodes must be positioned using the `Cartesian coordinate system <http://en.wikipedia.org/wiki/Cartesian_coordinate_system>`_. Not really what I'm looking for since, ideally, I'd like to be able to simply define the graph's structure and not have to worry about explicitly positioning each object.

* `PGF ("portable graphics format") LaTeX package <http://www.ctan.org/tex-archive/help/Catalogue/entries/pgf.html>`_: A LaTeX package for creating graphics, similar to PStricks. Unlike PStricks, this package has built-in support for Petri nets. It provides out-of-the box support for defining place and transition nodes, drawing edges, and placing a defined number of tokens within a node.  If interested, Section 3 of the `PGF manual <http://mirror.ctan.org/graphics/pgf/base/doc/generic/pgf/pgfmanual.pdf>`_ provides an excellent and detailed tutorial on building a Petri net graph. While the syntax for describing a graph seems easier than the petri-nets package above, there are still cumbersome aspects to the definition. For example, positioning is still a bit of a manual process. You must specify "left of", "right of", "above of", and "below of" layout commands for each node, though at least you do not have to deal with coordinates. Now granted, precise layout might be exactly what's needed in a LaTeX document, but I'm looking for something easier and faster to define.

* `Graphviz <http://www.graphviz.org/>`_: visualization software that can auto-generate directed graphs a specification given in its `DOT language <http://www.graphviz.org/content/dot-language>`_.  It's easy to create the place and transition nodes using subgraph declarations, and connecting the nodes requires no manual positioning at all.  Below is an example graph:

  File ``barber.dot``:

  .. sourcecode:: dot

      digraph G {
          subgraph place {
              graph [shape=circle,color=gray];
              node [shape=circle,fixedsize=true,width=2];
              "customer waiting";
              cutting;
              "idle barber";
              "customer paying";
          }
          subgraph transitions {
              node [shape=rect,height=0.2,width=2];
              enter;
              "start cutting";
              "finish cutting";
              exit;
          }
          enter -> "customer waiting";
          "customer waiting" -> "start cutting";
          "idle barber" -> "start cutting";
          "start cutting" -> cutting;
          cutting -> "finish cutting";
          "finish cutting" -> "customer paying";
          "finish cutting" -> "idle barber";
          "customer paying" -> exit;
      }       

  When run through dot::

      dot -T png barber.dot -o barber.png

  ...you get an image that looks something like this:

  .. image:: /blog/2011/03/14/barber.png
      :alt: Petri net barber example

  However, there doesn't seem to be a very good way for drawing the tokens within the place nodes.  Nodes can define text labels, but using periods for tokens isn't very readable. Alternatively, since nodes can specify a background image, you could generate a few images with different numbers of tokens and use the correct image to represent the desired number of tokens, while still having a text label too.

  If you intend to put your graph into LaTeX, check out the `dot2tex <http://www.fauskes.net/code/dot2tex/>`_ tool (written in Python) which will convert Graphviz's xdot output to LaTeX using PSTricks and PGF. Not only does this allow you to create beautiful graphs in your LaTeX documents, but it also allows your labels to be typeset by LaTeX (including the use of math mode, which is very nice).

* Finally, there's your favorite drawing or image manipulation program. It's quick-and-dirty, but perhaps the easiest solution I examined.  While it might be a pain to edit or reconnect nodes, it's certainly easy to set the positioning you want. If your program supports snapping edges and labels to nodes, such as `OODraw <http://www.openoffice.org/product/draw.html>`_, then moving or reconfiguring portions of the graph are even easier.


If you've found a better tool for drawing Petri nets, please share your experiences.
