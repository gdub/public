=================================================================================
Fix Samsung Captivate (Galaxy S) battery life after update to Android 2.2 (Froyo)
=================================================================================


The Update
==========

Finally, `Samsung has released`__ the `Android 2.2 Froyo <http://developer.android.com/sdk/android-2.2-highlights.html>`_ update for the Galaxy S (`AT&T Captivate`__). For reference, before the update I was getting about one-and-a-half days of battery life with normal usage (wi-fi and sync always on). Android 2.2 touts a 2x-5x improvement in CPU performance and improved memory management, both contributing to faster and smoother performance.

Sold! Where do I upgrade?

.. __: http://www.samsung.com/us/froyoupgrade2_2/captivate
.. __: http://www.amazon.com/gp/redirect.html?ie=UTF8&location=http%3A%2F%2Fwww.amazon.com%2FSamsung-Captivate-Android-Phone-AT%2Fdp%2FB003TLMQG8%2F&tag=listfrenzy-20&linkCode=ur2&camp=1789&creative=9325


The Aftermath
=============
Unfortunately, shortly after installing the update I notice an unacceptable battery drain rate. From a full change and with, at most, 10 minutes of use, my battery was dead about eight hours later. Wait a second, I thought performance was supposed to be better! Did this improved performance mean I had to sacrifice to the battery gods? This is not at all what I signed up for.

Maybe the first charge cycle was a fluke, I told myself. Well, it wasn't.  Two more charge cycles came and went, and I was still experiencing no more than about eight to ten hours of battery life.  At this point, I'm searching Android forums for solutions and finding many similar battery life horror stories.  It wasn't just Samsung Galaxy phones either. Reports about other HTC, Samsung, etc. phones were also prevalent. However, I could find no definitive solution to the problem.  Suggestions ranged everywhere from get a new battery to perform a factory reset to turn off wi-fi and many, many more.  At this point, I'm seriously considering reverting back to Android 2.1 (or rooting and going to 2.3.4), because less than half a day's worth a battery is no way to live.


The Solution
============
Then, there was one suggested solution I came across that actually seemed plausible.  I'm sure as hell not going to reset the phone and spend the time reloading all my data and apps, and nothing is wrong with my 6-month old battery as it was working just fine before the update to 2.2.

**The suggested solution was to reset the battery calibration table.** The theory here was that the software estimates the battery usage and life, and is what tells the phone what percentage the battery is at and when the phone should shut down due to critically-low remaining capacity. By reseting the calibration table, the software would relearn the battery capacity.  OK, I buy that, so how do I do this? Well, the suggestion was to install the `Battery Spy <https://play.google.com/store/apps/details?id=com.vertiform.android.app.batteryspy>`_ app, open the app, then follow: Menu button -> Battery Profile -> Reset calibration table -> Yes. Afterwards, you must let the battery drain all the way down, then charge it back to full (I did this with the phone off), and then let it charge all the way down again.


The Numbers
===========
Once changed to full, I performed a couple more actions: I turned off auto -sync and also changed the wi-fi sleep policy option to turn off wireless when the screen is inactive (Menu -> Settings -> Wireless and network -> Wi-Fi settings -> Menu -> Advanced -> Wi-Fi sleep policy -> When screen turns off).  The last option here is a default policy that was changed in Android 2.2; previously, "When screen turns off" was the default, but in 2.2 the default changed to "Never". From what I could gather, the "When screen turns off" setting was a problem because if the phone sat idle (e.g. while you were asleep) it would switch to 3G (even if it had excellent wi-fi signal) and potentially rack up unintended data usage. Another reason for the settings change, again from what I could gather, was that the wi-fi transmitter uses less power than the cell radio, and less power = more battery life.

To my astonishment, **4 days 22 hours and 14 minutes** later the phone was down to 2% battery, but still on despite normal usage and not ever going near a charger.  Wow, this is unbelievable!  Nearly five days of charge, that was more than twice the battery life I've ever experienced on this phone; but, was this due to the battery calibration or the settings I changed?  Time to test. I next charged the phone up to 100% again, then turned auto-sync back on.  1 day 20 hours and 45 minutes later, battery is at 5%.  OK, big hit there, but still better than I had ever experienced before the upgrade to Android 2.2.  Next, I changed fully once more, and then set the wi-fi sleep policy back to "Never". That time, 2 days 6 hours and 11 minutes later, it's at 4% charge.


Conclusion
==========
Your mileage may vary, but it seems that the battery calibration is what did the trick. At over two days of average battery life now, I'm once again a happy Android user and am enjoying a bit faster performance to boot.  Before I wrap this up, I'll leave you with one last tip: the `CPU Spy <https://play.google.com/store/apps/details?id=com.bvalosek.cpuspy>`_ app. This nice little app gives you a breakdown of how much time the CPU has spent in its various clock speeds (e.g. 1000, 800, 400, 200, and 100 MHz, and deep sleep), and the far majority of the phone's time should be in the deep sleep mode.  If it's not, then there's likely some app or background process that is keeping the phone from entering this low-power state.  Find out what's keeping it out of deep sleep, and your battery will thank you.  Hope this was helpful, and I would definitely try the battery recalibration before taking more drastic measures that involve losing all your apps and data.
